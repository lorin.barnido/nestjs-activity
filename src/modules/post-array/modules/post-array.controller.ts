import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { Modules\postArrayService } from './modules\post-array.service';
import { CreateModules\postArrayDto } from './dto/create-modules\post-array.dto';
import { UpdateModules\postArrayDto } from './dto/update-modules\post-array.dto';

@Controller('modules\post-array')
export class Modules\postArrayController {
  constructor(private readonly modules\postArrayService: Modules\postArrayService) {}

  @Post()
  create(@Body() createModules\postArrayDto: CreateModules\postArrayDto) {
    return this.modules\postArrayService.create(createModules\postArrayDto);
  }

  @Get()
  findAll() {
    return this.modules\postArrayService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.modules\postArrayService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateModules\postArrayDto: UpdateModules\postArrayDto) {
    return this.modules\postArrayService.update(+id, updateModules\postArrayDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.modules\postArrayService.remove(+id);
  }
}
