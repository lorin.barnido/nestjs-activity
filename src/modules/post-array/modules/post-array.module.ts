import { Module } from '@nestjs/common';
import { Modules\postArrayService } from './modules\post-array.service';
import { Modules\postArrayController } from './modules\post-array.controller';

@Module({
  controllers: [Modules\postArrayController],
  providers: [Modules\postArrayService]
})
export class Modules\postArrayModule {}
