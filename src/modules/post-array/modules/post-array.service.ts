import { Injectable } from '@nestjs/common';
import { CreateModules\postArrayDto } from './dto/create-modules\post-array.dto';
import { UpdateModules\postArrayDto } from './dto/update-modules\post-array.dto';

@Injectable()
export class Modules\postArrayService {
  create(createModules\postArrayDto: CreateModules\postArrayDto) {
    return 'This action adds a new modules\postArray';
  }

  findAll() {
    return `This action returns all modules\postArray`;
  }

  findOne(id: number) {
    return `This action returns a #${id} modules\postArray`;
  }

  update(id: number, updateModules\postArrayDto: UpdateModules\postArrayDto) {
    return `This action updates a #${id} modules\postArray`;
  }

  remove(id: number) {
    return `This action removes a #${id} modules\postArray`;
  }
}
