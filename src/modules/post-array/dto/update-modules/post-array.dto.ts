import { PartialType } from '@nestjs/mapped-types';
import { CreateModules\postArrayDto } from './create-modules\post-array.dto';

export class UpdateModules\postArrayDto extends PartialType(CreateModules\postArrayDto) {}
