import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Modules\postArrayModule } from './modules/post-array/modules/post-array.module';

@Module({
  imports: [Modules\postArrayModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
